{{ with secret "pki_int_hamsterwheel/issue/nomad-hashistack-tls" "common_name=client.local.nomad" "ip_sans=127.0.0.1" "ttl=1h" }}
{{ .Data.certificate }}
{{ end }}