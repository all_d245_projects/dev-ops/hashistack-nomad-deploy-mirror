#!/bin/bash
set -e

check_tls_certs() {
    if [ ! -f "$VAULT_TLS_CA" ] || [ ! -f "$VAULT_TLS_CERT" ] || [ ! -f "$VAULT_TLS_KEY" ]; then
        echo "Required Vault TLS files don't exist. Aborting";
        exit 1;
    fi
    return;
}

check_hashistack_connection() {
    test_vault_connection
    test_nomad_client_connection
    return;
}

test_vault_connection() {
    echo "Testing connection with Hashistack Vault...";
    if ! test_can_connect ${HASHISTACK_VAULT_IP} ${HASHISTACK_VAULT_PORT}; then
        info "Unable to connect with Hashistack Vault. Aborting!";
        exit 1;
    fi
    return;
}

test_nomad_client_connection() {
    echo "Testing connection with Hashistack Nomad...";
    if ! test_can_connect ${HASHISTACK_NOMAD_IP} ${HASHISTACK_NOMAD_PORT}; then
        info "Unable to connect with Hashistack Nomad. Aborting!";
        exit 1;
    fi
    return;
}

test_can_connect() {
    if nc -v -z -w 3 $1 $2 2>/dev/null; then
        echo "✓ connection Reachable";
    else
        echo "✗ connection Unreachable";
        return 1;
    fi
}

get_nomad_certs() {
    echo "Retrieving Nomad certificates...";
    consul-template -config ${CONSUL_TEMPLATE_PATH}/nomad-client-tls.hcl
    return;
}

check_tls_certs
check_hashistack_connection
get_nomad_certs