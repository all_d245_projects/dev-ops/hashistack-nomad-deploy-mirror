####################################################
# Hashistack - Nomad Deploy Image
#####################################################
FROM ubuntu:20.04

LABEL maintainer="Aldrich Saltson (asaltson@gmail.com)"

# Set build arguments
ARG NOMAD_TLS_SERVER_NAME
ARG HASHISTACK_NOMAD_IP
ARG HASHISTACK_NOMAD_PORT
ARG HASHISTACK_VAULT_IP
ARG HASHISTACK_VAULT_PORT
ARG VAULT_SERVER_NAME

# Set environment variables
ENV HASHISTACK_NOMAD_IP=${HASHISTACK_NOMAD_IP} \
    HASHISTACK_NOMAD_PORT=${HASHISTACK_NOMAD_PORT} \
    HASHISTACK_VAULT_IP=${HASHISTACK_VAULT_IP} \
    HASHISTACK_VAULT_PORT=${HASHISTACK_VAULT_PORT} \
    NOMAD_TLS_SERVER_NAME=${NOMAD_TLS_SERVER_NAME} \
    NOMAD_ADDR="https://${HASHISTACK_NOMAD_IP}:${HASHISTACK_NOMAD_PORT}" \
    VAULT_ADDR="https://${HASHISTACK_VAULT_IP}:${HASHISTACK_VAULT_PORT}" \
    VAULT_SERVER_NAME=${VAULT_SERVER_NAME} \
    CONSUL_TEMPLATE_PATH=/hashistack-nomad-deploy/consul-templates \
    TLS_CERTS_PATH=/hashistack-nomad-deploy/certs \
    VAULT_TLS_CA=${TLS_CERTS_PATH}/vault/Hashistack_Vault_CA_Root.pem \
    VAULT_TLS_CERT=${TLS_CERTS_PATH}/vault/vault.hashistack.hamsterwheel.xyz.bundle.pem \
    VAULT_TLS_KEY=${TLS_CERTS_PATH}/vault/vault.hashistack.hamsterwheel.xyz.key

# Prepare directories
RUN mkdir -p ${CONSUL_TEMPLATE_PATH}/nomad
RUN mkdir -p ${TLS_CERTS_PATH}/certs/nomad
RUN mkdir -p ${TLS_CERTS_PATH}/certs/vault

# Copy consul-template template files
COPY consul-templates/nomad/nomad-client-tls.hcl ${CONSUL_TEMPLATE_PATH}/nomad

# Copy certs-template template files
COPY certs-templates/nomad/* ${TLS_CERTS_PATH}/nomad

# Update package repositories and install packages
RUN apt-get update && \
    apt-get install -y \ 
    openssl curl

# Install Hashi-up
RUN curl -sLS https://get.hashi-up.dev | sh

# Install Nomad
RUN hashi-up nomad get \
    --dest /usr/local/bin/ \
    --extract true && \
    nomad --version

# Install Levant
RUN hashi-up levant get \
    --dest /usr/local/bin/ \
    --extract true && \
    levant --version

# Install Consul Template
RUN hashi-up consul-template get \
    --dest /usr/local/bin/ \
    --extract true && \
    consul-template --version

# Cleaning doanloaded packages
RUN apt-get clean

CMD ["bash"]