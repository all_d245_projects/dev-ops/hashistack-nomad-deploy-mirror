vault {
  address      = "${VAULT_ADDR}"
  unwrap_token = false
  renew_token  = false

  ssl {
    enabled     = true
    cert        = "${VAULT_TLS_CERT}"
    key         = "${VAULT_TLS_KEY}"
    ca_cert     = "${VAULT_TLS_CA}"
    server_name = "${VAULT_SERVER_NAME}"
  }
}

## NOMAD TLS CA ##
template {
  source      = "${CONSUL_TEMPLATE_PATH}/nomad/nomad-ca.pem.tpl"
  destination = "${TLS_CERTS_PATH}/nomad/nomad-ca.pem"
  command     = "bash -c '[ -f ${TLS_CERTS_PATH}/nomad/nomad-ca.pem ] && echo nomad-ca retrieved from vault || echo nomad-ca could not be retrieved && exit 1'"
}

## NOMAD TLS-KEY ##
template {
  source      = "${CONSUL_TEMPLATE_PATH}/nomad/nomad-key.pem.tpl"
  destination = "${TLS_CERTS_PATH}/nomad/nomad-key.pem"
  perms       = 0700
  command     = "bash -c '[ -f ${TLS_CERTS_PATH}/nomad/nomad-key.pem ] && echo nomad-key retrieved from vault || echo nomad-key could not be retrieved && exit 1'"
}

## NOMAD TLS-CERT ##
template {
  source      = "${CONSUL_TEMPLATE_PATH}/nomad/nomad-cert.pem.tpl"
  destination = "${TLS_CERTS_PATH}/nomad/nomad-cert.pem"
  perms       = 0700
  command     = "bash -c '[ -f ${TLS_CERTS_PATH}/nomad/nomad-cert.pem ] && echo nomad-cert retrieved from vault || echo nomad-cert could not be retrieved && exit 1'"
}