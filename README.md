# Hashistack Nomad Deploy



## Getting started

This Docker image enables CI/CD deploy jobs to deploy Nomad jobs to the Hashistack cluster.
The Image is bundled with the binaries `nomad`, `levant` and `consul-template`.

**Note:** Using the Hashistack Nomad deployer requires network access to the tailscale Virtual Private Network.

## Build arguments
This Docker image requires some build arguments set:

- `NOMAD_TLS_SERVER_NAME`: The TLS server name used to communicate with the Hashistack Vault client API.
- `HASHISTACK_NOMAD_IP`: The Hashistack Nomad Client API IP.
- `HASHISTACK_NOMAD_PORT`: The Hashistack Nomad CLient API Port.
- `HASHISTACK_VAULT_IP`: The Hashistack Vault Client API IP.
- `HASHISTACK_VAULT_PORT`: The Hashistack Vault CLient API Port.
- `VAULT_SERVER_NAME`: The TLS server name used to communicate with the Hashistack Vault client API.

## Environment variables
Containers built with this image require some envirionment variables set:
- `VAULT_TOKEN`: Token to permit communication with the Hashistack Vault API.
- `NOMAD_TOKEN`: Token to permit communication with the Hashistack Nomad AIP.

Containers built with this image will require volume bind mounts set for the Vault TLS certificates:
- path: `/hashistack-nomad-deploy/certs/vault/vault.hashistack.hamsterwheel.xyz.bundle.pem`.
- path: `/hashistack-nomad-deploy/certs/vault/vault.hashistack.hamsterwheel.xyz.key`.
- path: `/hashistack-nomad-deploy/certs/vault/Hashistack_Vault_CA_Root.pem`.

## Executing in a gitlab Job
``` yaml
deploy_job:
  image: cgbaker/hashistack-nomad-deploy
  before_script:
    - echo "Prepare Vault TLS files..."
    - cp path/to/vault.hashistack.hamsterwheel.xyz.bundle.pem /hashistack-nomad-deploy/certs/vault/vault.hashistack.hamsterwheel.xyz.bundle.pem
    - cp path/to/vault.hashistack.hamsterwheel.xyz.bundle.key /hashistack-nomad-deploy/certs/vault/vault.hashistack.hamsterwheel.xyz.bundle.key
    - cp path/to/Hashistack_Vault_CA_Root.pem /hashistack-nomad-deploy/certs/vault/Hashistack_Vault_CA_Root.pem
    - echo "Fetch and run init script..."
    - curl -L -o init.sh --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/projects/34238656/repository/files/init%2Esh/raw?ref=main
    - >
      if [ -f "/init.sh" ]; then
          echo "Initialization file retrieved.";
      else 
          echo "Initialization file could not be retrieved...";
          exit 1;
      fi
    - chmod +x ./init.sh
    - ./init.sh

  script:
    - "YOUR SCRIPT"
  variables:
    VAULT_TOKEN: "YOUR VAULT TOKEN"
    NOMAD_TOKEN: "YOUR NOMAD TOKEN"
```
